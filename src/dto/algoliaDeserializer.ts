export interface AlgoliaDeserializer<T> {
  
  deserialize(json: { [s: string]: any }): T|undefined;

}
