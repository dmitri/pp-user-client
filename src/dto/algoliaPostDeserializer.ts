import {AlgoliaDeserializer, Post, Position, Color} from './postplaneDto';

export class AlgoliaPostDeserializer implements AlgoliaDeserializer<Post> {

  deserialize(hit: any): Post {
    const post = new Post();
    post.text = hit.text;
    post.position = new Position(hit.x, hit.y);
    post.displayName = hit.displayName;
    post.planeName = hit.planeName;
    post.planeOwner = hit.planeOwner;
    post.renderId = hit.renderId;
    post.color = new Color(hit.hue, hit.saturation, hit.lightness);
    post.serverTimeMillis = hit.serverTimeMillis;
    return post;
  }

}
