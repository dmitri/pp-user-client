export interface ArrangeParameters {
  readonly spacing: number;
  readonly stackSize: number;
  readonly repeatCount: number;
}
