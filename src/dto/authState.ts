export interface AuthState {
  
  readonly hasReceivedFirstAuthMessage: boolean;
  readonly isLoggedIn: boolean
  readonly userId: string;
  readonly hasReceivedUsernameResponse: boolean;
  readonly username?: string;
  readonly email: string;
  readonly emailVerified: boolean;

}
