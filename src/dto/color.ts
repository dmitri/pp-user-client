export class Color {
  constructor(readonly hue: number,
    readonly saturation: number,
    readonly lightness: number) {}

  copy() {
    return new Color(this.hue, this.saturation, this.lightness);
  }

  toCssString() {
    return 'hsl(' 
      + this.hue + ',' 
      + this.saturation + '%,'
      + this.lightness + '%)';
  }

  toCssStringWithAlpha(a: number) {
    return 'hsla(' 
      + this.hue + ',' 
      + this.saturation + '%,'
      + this.lightness + '%, ' + a + ')';
  }

  equals(otherColor: Color) {
    return otherColor.hue === this.hue
      && otherColor.saturation === this.saturation
      && otherColor.lightness === this.lightness;
  }

  static Black: Color = new Color(0, 0, 0);
}
