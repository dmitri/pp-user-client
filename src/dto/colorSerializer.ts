import {Color, TypeSerializer} from './postplaneDto';

export class ColorSerializer implements TypeSerializer<Color> {

  serialize(c: Color): {[s: string]: any} {
    return { 
      hue: c.hue, 
      saturation: c.saturation, 
      lightness: c.lightness 
    };
  }

  public deserialize(rawColor: {[s:string]: any}): Color|undefined {
    return new Color(rawColor.hue, rawColor.saturation, rawColor.lightness);
  }

}