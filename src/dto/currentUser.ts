import {Color, DatabaseIdentity} from '../dto/postplaneDto';

export class CurrentUser implements DatabaseIdentity {
  
  constructor(
    public readonly connectionId: string,
    public readonly userId: string,
    public readonly displayName: string, 
    public readonly color: Color,
    public readonly lastUpdateMillis: any,
    public readonly databaseId?: string) {}  
}
