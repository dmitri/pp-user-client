import {ColorSerializer, TypeSerializer} from '../dto/postplaneDto';

import {CurrentUser} from './postplaneDto';

export class CurrentUserSerializer implements TypeSerializer<CurrentUser> {

  private color = new ColorSerializer();

  public serialize(c: CurrentUser): {[s: string]: any} {
    return {
      connectionId: c.connectionId,
      userId: c.userId,
      displayName: c.displayName,
      color: this.color.serialize(c.color),
      lastUpdateMillis: c.lastUpdateMillis
    };
  }

  public deserialize(json: {[s:string]: any}): CurrentUser|undefined {
    return new CurrentUser(json.connectionId, json.userId, json.displayName, this.color.deserialize(json.color), json.lastUpdateMillis);
  }

}
