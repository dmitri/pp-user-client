export interface DatabaseIdentity {
  readonly databaseId?: string;
}
