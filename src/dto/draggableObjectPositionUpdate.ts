import {Position} from './postplaneDto';

export interface DraggableObjectPositionUpdate {
  readonly delta: Position;
  readonly objectId: string;
}
