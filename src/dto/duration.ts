import {DurationUnit} from './postplaneDto';

export interface Duration {
  readonly number: number;
  readonly unit: DurationUnit;
}
