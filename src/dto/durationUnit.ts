const enum DurationUnit {
  second = 'second',
  minute = 'minute',
  hour = 'hour',
  day = 'day',
  week = 'week',
  year = 'year'
}

class DurationUnitSerializer {

  serialize(unit: DurationUnit): string {
    return unit;
  }

  deserialize(s: string): DurationUnit {
    switch(s) {
    case DurationUnit.second: return DurationUnit.second;
    case DurationUnit.minute: return DurationUnit.minute;
    case DurationUnit.hour: return DurationUnit.hour;
    case DurationUnit.day: return DurationUnit.day;
    case DurationUnit.week: return DurationUnit.week;
    case DurationUnit.year: return DurationUnit.year;
    default: return DurationUnit.day;
    }
  }
}

export {DurationUnit, DurationUnitSerializer};
