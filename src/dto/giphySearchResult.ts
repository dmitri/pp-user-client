import { DatabaseIdentity } from "./postplaneDto";

interface GiphyImage {
  webp?: string;
  url: string;
  width: number;
  height: number;
}

interface GiphySearchResult extends DatabaseIdentity {
  images: { 
    fixed_width_small: GiphyImage,
    original: GiphyImage
  }
}

interface GiphySearchResults {
  data: GiphySearchResult[];
  pagination: {
    count: number,
    offset: number,
    total_count: number
  }
}

export {GiphyImage, GiphySearchResult, GiphySearchResults}