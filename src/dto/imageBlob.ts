import {Position} from './postplaneDto';

export interface ImageBlob {
  readonly size: Position;
  readonly blob: Blob;
}
