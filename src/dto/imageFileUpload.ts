import {Position} from './postplaneDto';

export interface ImageFileUpload {
  readonly size: Position;
  readonly url: string;
}
