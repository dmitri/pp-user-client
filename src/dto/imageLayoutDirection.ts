export const enum ImageLayoutDirection {
  Up, Down, Left, Right
}
