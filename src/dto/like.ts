import { DatabaseIdentity, PostIdentifier, PublicUserInfo, TypeSerializer, Post, PostSerializer} from './postplaneDto';

interface Like extends DatabaseIdentity {
  readonly userinfo: PublicUserInfo;
  readonly atTime: Date|Object;
}

class LikeSerializer implements TypeSerializer<Like>  {
  serialize(like: Like): {[s: string]: any} {
    return like;
  }

  deserialize(json: {[s: string]: any}): Like {
    return {
      userinfo: json.userinfo,
      atTime: json.atTime
    }
  }

}


interface LikeBase extends DatabaseIdentity {
  readonly forPost: PostIdentifier;
  readonly post: Post;
  readonly atMillis: number;
}

class LikeBaseSerializer implements TypeSerializer<LikeBase> {

  private readonly postSerializer = new PostSerializer();

  serialize(likeBase: LikeBase): {[s: string]: any} {
    return {
      post: this.postSerializer.serialize(likeBase.post),
      forPost: likeBase.forPost
    };
  }

  deserialize(json: {[s: string]: any}): LikeBase {
    return {
      post: this.postSerializer.deserialize(json.post),
      forPost: json.forPost,
      atMillis: json.atMillis
    }
  }

}

interface UserLike extends LikeBase {
  readonly userId: string;
}

class UserLikeSerializer implements TypeSerializer<UserLike> {

  private readonly likeBaseSerializer = new LikeBaseSerializer();

  serialize(like: UserLike): {[s: string]: any} {
    return Object.assign(
      this.likeBaseSerializer.serialize(like),
      { userId: like.userId }
    );
  }

  deserialize(json: {[s: string]: any}): UserLike {
    return Object.assign(
      this.likeBaseSerializer.deserialize(json),
      { userId: json.userId }
    );
  }

}

interface LikeCount extends LikeBase {
  readonly count: number;
}

class LikeCountSerializer implements TypeSerializer<LikeCount> {
  
  private readonly likeBaseSerializer = new LikeBaseSerializer();

  serialize(likeCount: LikeCount): {[s: string]: any} {
    return Object.assign(
      this.likeBaseSerializer.serialize(likeCount),
      {
        count: likeCount.count,
      }
    );
  }

  deserialize(json: {[s: string]: any}): LikeCount {
    return Object.assign(
      this.likeBaseSerializer.deserialize(json),
      {
        count: json.count,
      }
    );
  }
}

export {Like, LikeSerializer, LikeCount, LikeCountSerializer, UserLike, UserLikeSerializer};
