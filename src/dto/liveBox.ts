import {Color, Rectangle, DatabaseIdentity} from './postplaneDto';

export class LiveBox implements DatabaseIdentity {
  
  constructor(
    public readonly planeRect: Rectangle, 
    public readonly connectionId: string, 
    public readonly color: Color, 
    public readonly databaseId?: string) {}
  
}
