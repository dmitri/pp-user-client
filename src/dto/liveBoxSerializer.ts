import {TypeSerializer, LiveBox, ColorSerializer, RectangleSerializer} from './postplaneDto';

export class LiveBoxSerializer implements TypeSerializer<LiveBox> {

  private readonly color = new ColorSerializer();
  private readonly rectangle = new RectangleSerializer();

  serialize(b: LiveBox): {[s: string]: any} {
    return {
      planeRect: this.rectangle.serialize(b.planeRect),
      connectionId: b.connectionId,
      color: this.color.serialize(b.color)
    };
  }

  public deserialize(json: {[s:string]: any}): LiveBox|undefined {
    return new LiveBox(this.rectangle.deserialize(json.planeRect), json.connectionId, this.color.deserialize(json.color));
  }

}
