export enum ModalName {

  PostMenu = 'post',
  PostLikes = 'postLikes',
  Logout = 'logout',
  Login = 'login',
  Username = 'username',
  Color = 'color',
  Profile = 'profile',
  LineWidth = 'lineWidth',
  Pattern = 'pattern',
  NewPlane = 'newPlane',
  ImageLayout = 'imageLayout',
  MultiPostEdit = 'multiPostEdit',
  Sticker = 'sticker',
  Error = 'error'
}
