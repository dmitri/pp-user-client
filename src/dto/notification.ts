import {NotificationType, Color, PostIdentifier} from './postplaneDto';

interface Notification {
  readonly databaseId?: string;
  readonly type: NotificationType;
  readonly atMillis: number;
  readonly isClicked: boolean;
  readonly userId: string;
  readonly color: Color;
}

interface NewPrivatePlaneMembershipNotification extends Notification {
  readonly planeOwner: string;
  readonly planeName: string;
}

interface NotificationForPost extends Notification {
  readonly forPost: PostIdentifier;
  readonly text: string;
  readonly username: string;
}

interface MentionedNotification extends NotificationForPost {}

interface PostLikedNotification extends NotificationForPost {}

interface PostRepliedToNotification extends NotificationForPost {
  readonly repliedToPost: PostIdentifier
}

export {Notification, NewPrivatePlaneMembershipNotification, MentionedNotification, PostRepliedToNotification, NotificationForPost, PostLikedNotification}
