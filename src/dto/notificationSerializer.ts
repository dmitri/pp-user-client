import {ColorSerializer, NewPrivatePlaneMembershipNotification, MentionedNotification, PostRepliedToNotification, RectangleSerializer, TypeSerializer, Notification, NotificationType, NotificationTypeSerializer} from './postplaneDto';

export class NotificationSerializer implements TypeSerializer<Notification> {

  private readonly colorSerializer = new ColorSerializer();

  public serialize(n: Notification): {[s: string]: any} {
    return undefined;
  }
  

  public deserialize(json: {[s:string]: any}, databaseId?: string): Notification|undefined {

    const type = NotificationTypeSerializer[json.type];
    switch(type) {
    case NotificationType.UserAddedToPrivatePlane:
      const n: NewPrivatePlaneMembershipNotification = {
        userId: json.userId,
        databaseId: databaseId,
        type: type,
        atMillis: json.atMillis,
        color: this.colorSerializer.deserialize(json.color),
        planeName: json.planeName,
        planeOwner: json.planeOwner,
        isClicked: json.isClicked
      };
      return n;

    case NotificationType.UserMentionedInPost:
      const m: MentionedNotification = {
        userId: json.userId,
        databaseId: databaseId,
        type: type,
        atMillis: json.atMillis,
        color: this.colorSerializer.deserialize(json.color),
        username: json.username,
        isClicked: json.isClicked,
        forPost: json.forPost,
        text: json.text
      };
      return m;

    case NotificationType.UserPostRepliedTo:
      const o: PostRepliedToNotification = {
        userId: json.userId,
        databaseId: databaseId,
        type: type,
        atMillis: json.atMillis,
        isClicked: json.isClicked,
        text: json.text,
        username: json.username,
        color: this.colorSerializer.deserialize(json.color),
        repliedToPost: json.repliedToPost,
        forPost: json.forPost
      }
      return o;

    case NotificationType.UserPostLiked:
      const p: MentionedNotification = {
        userId: json.userId,
        databaseId: databaseId,
        type: type,
        atMillis: json.atMillis,
        color: this.colorSerializer.deserialize(json.color),
        username: json.username,
        isClicked: json.isClicked,
        forPost: json.forPost,
        text: json.text
      };
      return p;

    default:
      console.error('Weird got unknown type in Notification deserialize', json);
      return undefined;
    }

  }

}
