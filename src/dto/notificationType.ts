const enum NotificationType {
  UserAddedToPrivatePlane = 'UserAddedToPrivatePlane',
  UserMentionedInPost = 'UserMentionedInPost',
  UserPostRepliedTo = 'UserPostRepliedTo',
  UserPostLiked = 'UserPostLiked'
}

const NotificationTypeSerializer: {[s: string]: NotificationType} = {
  UserAddedToPrivatePlane: NotificationType.UserAddedToPrivatePlane,
  UserMentionedInPost: NotificationType.UserMentionedInPost, 
  UserPostRepliedTo: NotificationType.UserPostRepliedTo,
  UserPostLiked: NotificationType.UserPostLiked
}

export {NotificationType, NotificationTypeSerializer}
