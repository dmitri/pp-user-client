import {DatabaseIdentity, TypeSerializer, PlaneIdentifier, PlaneSettings, PlaneSettingsSerializer} from './postplaneDto';

interface PlaneDocument extends DatabaseIdentity {
  readonly settings: PlaneSettings,
  readonly planeId: PlaneIdentifier
}

class PlaneDocumentSerializer implements TypeSerializer<PlaneDocument> {

  private readonly settingsSerializer = new PlaneSettingsSerializer();

  serialize(obj: PlaneDocument): {[s: string]: any} {
    return { 
      settings: this.settingsSerializer.serialize(obj.settings),
      planeId: obj.planeId
    };
  }

  public deserialize(raw: {[s:string]: any}): PlaneDocument|undefined {
    return {
      settings: this.settingsSerializer.deserialize(raw.settings),
      planeId: raw.planeId
    }
  }
}

export {PlaneDocument, PlaneDocumentSerializer};
