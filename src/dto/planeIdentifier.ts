export interface PlaneIdentifier {
  readonly planeOwner?: string;
  readonly planeName: string;
}
