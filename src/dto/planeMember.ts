import {Color, Post, PublicUserInfo} from './postplaneDto';

export interface PlaneMember {
  readonly latestPost?: Post;
  readonly color?: Color;
  readonly isConnected: boolean;
  readonly connectionCount: number;
  readonly userinfo: PublicUserInfo;
}
