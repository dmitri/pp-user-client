import {DatabaseIdentity, TypeSerializer} from './postplaneDto';

interface PlaneSettings {
  readonly isPubliclyViewable: boolean;
}

class PlaneSettingsSerializer implements TypeSerializer<PlaneSettings> {
  serialize(obj: PlaneSettings): {[s: string]: any} {
    return { 
      isPubliclyViewable: obj.isPubliclyViewable
    };
  }

  public deserialize(raw: {[s:string]: any}): PlaneSettings|undefined {
    return {
      isPubliclyViewable: raw.isPubliclyViewable
    }
  }
}

export {PlaneSettings, PlaneSettingsSerializer};
