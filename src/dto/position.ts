export class Position { 

  private readonly equalsEpsilon = Number.EPSILON * 10.0;

  constructor(readonly x: number, readonly y: number) {}
  
  equal(pt: Position): boolean {
    return this.distanceSquared(pt) < this.equalsEpsilon;
  }

  delta(pt: Position) {
    return new Position(pt.x - this.x, pt.y - this.y);
  }
  
  add(pt: Position) {
    return new Position(this.x + pt.x, this.y + pt.y);
  }
  
  isZero() {
    return this.x === 0 && this.y === 0;
  }
  
  distance(pt: Position) {
    return Math.sqrt(this.distanceSquared(pt));
  }

  distanceSquared(pt: Position) {
    const xDiff = this.x - pt.x;
    const yDiff = this.y - pt.y;
    return (xDiff * xDiff) + (yDiff * yDiff);
  }
  
  center(pt: Position) {
    return new Position((pt.x + this.x) / 2.0, (pt.y + this.y) / 2.0);
  }
  
  subtract(pt: Position) {
    return new Position(this.x - pt.x, this.y - pt.y);
  }

  scale(s: number) {
    return new Position(this.x * s, this.y * s);
  }

}
