import {TypeSerializer, Position} from './postplaneDto';

export class PositionSerializer implements TypeSerializer<Position> {

  serialize(p: Position): {[s: string]: any} {
    return {
      x: p.x,
      y: p.y
    }
  }

  deserialize(rawPosition: {[s:string]: any}): Position|undefined {
    return new Position(rawPosition.x, rawPosition.y);
  }
  
}