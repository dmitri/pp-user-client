import {Color, Position, Rectangle, Shape, UserPrefs, DatabaseIdentity, PlaneIdentifier, PostIdentifier, DurationUnit} from '../dto/postplaneDto';

export class Post implements DatabaseIdentity {

  userId: string;
  renderId: string;
  planeOwner: string = '';
  planeName: string;
  text: string = '';
  position: Position = new Position(0, 0);
  color: Color = new Color(0, 50, 0);
  displayName: string = '';
  serverTimeMillis: any = 0;
  durationStartMillis: any = 0;
  durationNumber: number = 0;
  durationUnit: DurationUnit = DurationUnit.day;
  durationMillis: number = 0;
  size: Position = new Position(0, 0);
  shapes: Shape[] = [];
  connectionId: string = '';
  bold: boolean = false;
  italic: boolean = false;
  caps: boolean = false;
  textSize: number = 16;
  databaseId: string = '';
  drawBoundingBox: Rectangle = new Rectangle(this.position, this.size);
  inReplyTo?: PostIdentifier;
  twitterOEmbed?: any;
  likeCount: number = 0;

  copyWithPrefs(prefs: UserPrefs) {
    const post = this.copy();
    post.color = prefs.color;
    post.textSize = prefs.textSize;
    post.bold = prefs.textBold;
    post.italic = prefs.textItalic;
    post.caps = prefs.textCaps;
    return post;
  }

  getPostIdentifier(): PostIdentifier {
    return {
      planeOwner: this.planeOwner,
      planeName: this.planeName,
      postId: this.databaseId
    };
  }

  copy() {
    const p = new Post();
    p.userId = this.userId;
    p.renderId = this.renderId;
    p.planeOwner = this.planeOwner;
    p.planeName = this.planeName;
    p.text = this.text;
    p.position = this.position;
    p.color = this.color;
    p.displayName = this.displayName;
    p.serverTimeMillis = this.serverTimeMillis;
    p.durationStartMillis = this.durationStartMillis;
    p.durationMillis = this.durationMillis;
    p.durationNumber = this.durationNumber;
    p.durationUnit = this.durationUnit;
    p.size = this.size;
    p.shapes = this.shapes;
    p.connectionId = this.connectionId;
    p.bold = this.bold;
    p.italic = this.italic;
    p.caps = this.caps;
    p.textSize = this.textSize;
    p.databaseId = this.databaseId;
    p.drawBoundingBox = this.drawBoundingBox;
    p.inReplyTo = this.inReplyTo;
    p.likeCount = this.likeCount;
    return p;
  }
}
