import {PlaneIdentifier} from './postplaneDto';

export interface PostIdentifier extends PlaneIdentifier {
  readonly postId: string;
}
