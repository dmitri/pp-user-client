import {Post, Color, ColorSerializer, Position, PositionSerializer, Rectangle, RectangleSerializer, TypeSerializer, Shape, ShapeSerializer, DurationUnitSerializer} from '../dto/postplaneDto';

export class PostSerializer implements TypeSerializer<Post> {

  private readonly color = new ColorSerializer();
  private readonly rectangle = new RectangleSerializer();
  private readonly position = new PositionSerializer();
  private readonly shape = new ShapeSerializer();
  private readonly durationUnitSerializer = new DurationUnitSerializer();


  serialize(post: Post): {[s: string]: any} {
    const x: {[s: string]: any} = {
      userId: post.userId,
      renderId: post.renderId,
      planeName: post.planeName,
      planeOwner: post.planeOwner,
      text: post.text,
      position: this.position.serialize(post.position),
      color: this.color.serialize(post.color),
      displayName: post.displayName,
      serverTimeMillis: post.serverTimeMillis,
      durationMillis: post.durationMillis,
      durationStartMillis: post.durationStartMillis,
      durationNumber: post.durationNumber,
      durationUnit: this.durationUnitSerializer.serialize(post.durationUnit),
      size: this.position.serialize(post.size),
      shapes: post.shapes.map(s => this.shape.serialize(s)),
      connectionId: post.connectionId,
      bold: post.bold,
      italic: post.italic,
      caps: post.caps,
      textSize: post.textSize,
      databaseId: post.databaseId
    }
    if (post.drawBoundingBox) {
      x.drawBoundingBox = this.rectangle.serialize(post.drawBoundingBox);
    }
    if (post.inReplyTo) {
      x.inReplyTo = post.inReplyTo;
    }

    return x;
  }

  private isDate(field: any): boolean {
    return field instanceof Date;
  }

  deserialize(rawPost: {[s: string]: any}): Post|undefined {
    if (! rawPost) {
      return undefined;
    }
    try {

      const post: Post = new Post();
      post.userId = rawPost.userId;
      post.renderId = rawPost.renderId;
      post.planeName = rawPost.planeName;
      post.planeOwner = rawPost.planeOwner;
      post.text = rawPost.text;
      post.position = this.position.deserialize(rawPost.position);
      post.color = rawPost.color ? this.color.deserialize(rawPost.color) : new Color(0, 100, 0);
      post.displayName = rawPost.displayName;
      if (this.isDate(rawPost.serverTimeMillis)) {
        post.serverTimeMillis = rawPost.serverTimeMillis.getTime();
      } else if (typeof(rawPost.serverTimeMillis) === 'number') {
        post.serverTimeMillis = rawPost.serverTimeMillis;
      } else {
        post.serverTimeMillis = Date.now();
      }
      post.durationMillis = rawPost.durationMillis;
      if (post.durationMillis) {
        if (this.isDate(rawPost.durationStartMillis)) {
          post.durationStartMillis = rawPost.durationStartMillis.getTime()
        } else if (typeof(rawPost.durationStartMillis) === 'number') {
          post.durationStartMillis = rawPost.durationStartMillis;
        } else {
          post.durationStartMillis = Date.now();
        }
      } else {
        post.durationStartMillis = 0;
      }
      post.durationNumber = rawPost.durationNumber;
      post.durationUnit = this.durationUnitSerializer.deserialize(rawPost.durationUnit);
      
      if (rawPost.size) {
        post.size = this.position.deserialize(rawPost.size);
      }
      post.shapes = rawPost.shapes ? rawPost.shapes.map((rawShape: Shape) => this.shape.deserialize(rawShape)) : [];
      post.connectionId = rawPost.connectionId;
      post.bold = rawPost.bold;
      post.italic = rawPost.italic;
      post.caps = rawPost.caps;
      post.textSize = rawPost.textSize;

      post.databaseId = rawPost.databaseId;

      if (rawPost.drawBoundingBox) {
        post.drawBoundingBox = this.rectangle.deserialize(rawPost.drawBoundingBox);
      } else {
        post.drawBoundingBox = undefined;
      }

      if (rawPost.inReplyTo) {
        post.inReplyTo = rawPost.inReplyTo;
      }

      if (rawPost.twitterOEmbed) {
        post.twitterOEmbed = rawPost.twitterOEmbed;
      }

      if (rawPost.likeCount) {
        post.likeCount = rawPost.likeCount;
      }

      return post;   
    } catch (e) {
      console.log('got exception from raw post', rawPost);
      console.error(e);
      return undefined;
    }
  }
}