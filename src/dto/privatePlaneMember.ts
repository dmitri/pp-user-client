import {PublicUserInfo, DatabaseIdentity, PrivatePlaneUserStatus} from '../dto/postplaneDto';


export interface PrivatePlaneMember extends DatabaseIdentity {
  readonly status: PrivatePlaneUserStatus;
  readonly updatedAtMillis: number|Object;
  readonly userinfo: PublicUserInfo;
}
