import {PublicUserInfoSerializer, TypeSerializer, PrivatePlaneMember, PrivatePlaneUserStatus} from '../dto/postplaneDto';

export class PrivatePlaneMemberSerializer implements TypeSerializer<PrivatePlaneMember> {

  private readonly publicUserinfoSerializer = new PublicUserInfoSerializer();

  serialize(p: PrivatePlaneMember): {[s: string]: any} {
    return {
      status: p.status,
      updatedAtMillis: p.updatedAtMillis,
      userinfo: this.publicUserinfoSerializer.serialize(p.userinfo)
    };
  }

  private isDate(field: any): boolean {
    return field instanceof Date;
  }

  deserialize(rawObj: {[s: string]: any}): PrivatePlaneMember|undefined {
    if (! rawObj) {
      return undefined;
    }
    return {
      status: this.deserializeStatus(rawObj.status),
      updatedAtMillis: this.isDate(rawObj.updatedAtMillis) ? rawObj.updatedAtMillis.getTime() : rawObj.updatedAtMillis,
      userinfo: this.publicUserinfoSerializer.deserialize(rawObj.userinfo)
    }
  }

  private deserializeStatus(raw: string) {
    switch (raw) {
      case 'None': return PrivatePlaneUserStatus.None;
      case 'HasRequestedAccess': return PrivatePlaneUserStatus.HasRequestedAccess;
      case 'AccessGranted': return PrivatePlaneUserStatus.AccessGranted;
      case 'AccessDenied': return PrivatePlaneUserStatus.AccessDenied;
      case 'IsOwner': return PrivatePlaneUserStatus.IsOwner;
      default: return PrivatePlaneUserStatus.None;
    }
  }

}
