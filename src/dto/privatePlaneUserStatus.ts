export const enum PrivatePlaneUserStatus {
  None = 'None',
  HasRequestedAccess = 'HasRequestedAccess',
  AccessGranted = 'AccessGranted',
  AccessDenied = 'AccessDenied',
  IsOwner = 'IsOwner',
  ReadOnly = 'ReadOnly'
}
