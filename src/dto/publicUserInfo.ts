import {DatabaseIdentity} from '../dto/postplaneDto';

export interface PublicUserInfo extends DatabaseIdentity {
  readonly userId: string;
  readonly username: string;
}
