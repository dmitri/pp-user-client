import {PublicUserInfo, TypeSerializer} from './postplaneDto';

export class PublicUserInfoSerializer implements TypeSerializer<PublicUserInfo> {

  serialize(p: PublicUserInfo): {[s: string]: any} {
    return {
      username: p.username,
      userId: p.userId
    };
  }

  deserialize(rawObj: {[s: string]: any}): PublicUserInfo|undefined {
    if (! rawObj) {
      return undefined;
    }
    return {
      username: rawObj.username,
      userId: rawObj.userId
    }
  }

}
