import {Position} from './position';

export class Rectangle {

  readonly bottomRightPosition: Position;
  readonly area: number;

  constructor(readonly position: Position, readonly size: Position) {
    this.bottomRightPosition = new Position(this.position.x + this.size.x, this.position.y + this.size.y);
    this.area = size.x * size.y;
  }

  toRTreeRectFormat(): { [s: string]: number } {
    return { 
      x: this.position.x, 
      y: this.position.y,
      w: this.size.x,
      h: this.size.y 
    };
  }

  static fromTwoPoints(p1: Position, p2: Position): Rectangle {
    let posX;
    let sizeX;
    if (p1.x < p2.x) {
      posX = p1.x;
      sizeX = p2.x - p1.x;
    } else {
      posX = p2.x;
      sizeX = p1.x - p2.x;
    }
    let posY;
    let sizeY;
    if (p1.y < p2.y) {
      posY = p1.y;
      sizeY = p2.y - p1.y;
    } else {
      posY = p2.y;
      sizeY = p1.y - p2.y;
    }
    return new Rectangle(new Position(posX, posY), new Position(sizeX, sizeY));
  }

  translate(delta: Position): Rectangle {
    return new Rectangle(this.position.add(delta), this.size);
  }

  equal(rect: Rectangle): boolean {
    return this.position.equal(rect.position) && this.size.equal(rect.size);
  }

  union(otherRect: Rectangle): Rectangle {
    const minTopLeft = new Position(Math.min(this.position.x, otherRect.position.x), Math.min(this.position.y, otherRect.position.y))
    const maxBottomRight = new Position(
      Math.max(this.bottomRightPosition.x, otherRect.bottomRightPosition.x),
      Math.max(this.bottomRightPosition.y, otherRect.bottomRightPosition.y)
    );
    return new Rectangle(minTopLeft, maxBottomRight.subtract(minTopLeft));
  }

  addPointToBoundingBox(position: Position): Rectangle {
    const minTopLeft = new Position(Math.min(this.position.x, position.x), Math.min(this.position.y, position.y))
    const maxBottomRight = new Position(
      Math.max(this.bottomRightPosition.x, position.x),
      Math.max(this.bottomRightPosition.y, position.y)
    );
    return new Rectangle(minTopLeft, maxBottomRight.subtract(minTopLeft));
  }

  intersects(otherRect: Rectangle): boolean {
    return this.bottomRightPosition.x > otherRect.position.x
      && otherRect.bottomRightPosition.x > this.position.x
      && this.bottomRightPosition.y > otherRect.position.y
      && otherRect.bottomRightPosition.y > this.position.y;
  }

  contains(otherRect: Rectangle): boolean {
    return (otherRect.position.x + otherRect.size.x) < (this.position.x + this.size.x)
      && (otherRect.position.x) > (this.position.x)
      && (otherRect.position.y) > (this.position.y)
      && (otherRect.position.y + otherRect.size.y) < (this.position.y + this.size.y);
  }

}
