import {TypeSerializer, PositionSerializer, Rectangle, Position} from './postplaneDto';

export class RectangleSerializer implements TypeSerializer<Rectangle> {

  private position = new PositionSerializer();
  
  serialize(r: Rectangle): {[s: string]: any} {
    return {
      position: this.position.serialize(r.position),
      size: this.position.serialize(r.size)
    };
  }

  deserialize(rawRect: {[s:string]: any}): Rectangle|undefined {
    return new Rectangle(this.position.deserialize(rawRect.position), this.position.deserialize(rawRect.size));
  }
  
}
