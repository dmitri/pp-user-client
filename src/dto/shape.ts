import {Color, Position} from './postplaneDto';

export class Shape {

    constructor(
      readonly shapeId: string,
      readonly shapeType: string,
      public points: Position[],
      readonly color: Color,
      readonly strokeWidth: number,
      readonly fill: boolean,
      readonly pattern: string
    ) {}

}
