import {Shape, Position, TypeSerializer, PositionSerializer, ColorSerializer} from '../dto/postplaneDto';

export class ShapeSerializer implements TypeSerializer<Shape> {

  private readonly position = new PositionSerializer();
  private readonly color = new ColorSerializer();

  serialize(s: Shape): {[s: string]: any} {
    return {
      shapeId: s.shapeId,
      shapeType: s.shapeType,
      points: s.points.map(p => this.position.serialize(p)),
      color: this.color.serialize(s.color),
      strokeWidth: s.strokeWidth,
      fill: s.fill,
      pattern: s.pattern
    };
  }

  public deserialize(rawShape: {[s:string]: any}): Shape|undefined {
    return new Shape(
      rawShape.shapeId, 
      rawShape.shapeType, 
      rawShape.points.map((rawPosition: Position) => this.position.deserialize(rawPosition)), 
      this.color.deserialize(rawShape.color),
      rawShape.strokeWidth,
      rawShape.fill,
      rawShape.pattern);
  }
  
}

