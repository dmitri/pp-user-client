import {TextPartType} from './postplaneDto';

export class TextPart {

  readonly imageUrl: string;

  constructor(readonly value: string, readonly type: TextPartType, readonly data?: any) {
    if (type === TextPartType.Instagram) {
      this.imageUrl = value + 'media/?size=l';
    } else if (type === TextPartType.Image) {
      this.imageUrl = value;
    } else if (type === TextPartType.Youtube) {
      this.imageUrl = '//img.youtube.com/vi/' + data.youtubeId + '/0.jpg'
    }
  }

  isImage(): boolean {
    return this.imageUrl !== undefined;
  }

}
