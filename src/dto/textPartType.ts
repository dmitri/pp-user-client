export const enum TextPartType {
  Image = 'image',
  Text = 'text',
  Url = 'url',
  Youtube = 'youtube',
  Instagram = 'instagram',
  Twitter = 'twitter'
}
