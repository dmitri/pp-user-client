export interface TypeSerializer<T> {
  
  serialize(obj: T): { [s: string]: any };
  deserialize(json: { [s: string]: any }, databaseId?: string): T|undefined;

}
