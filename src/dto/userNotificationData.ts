export interface UserNotificationData  {
  readonly unreadCount: number;
}
