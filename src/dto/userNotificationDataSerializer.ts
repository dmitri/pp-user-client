import {TypeSerializer, UserNotificationData} from './postplaneDto';

export class UserNotificationDataSerializer implements TypeSerializer<UserNotificationData> {
  
  serialize(x: UserNotificationData): {[s: string]: any} {
    return {
      unreadCount: x.unreadCount
    };
  }

  deserialize(x: {[s:string]: any}): UserNotificationData|undefined {
    return {
      unreadCount: x.unreadCount
    };
  }
  
}
