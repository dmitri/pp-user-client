import {Color, DurationUnit, GiphyImage} from './postplaneDto';

export class UserPrefs {
  constructor(
    public readonly color: Color,
    public readonly displayName: string,
    public readonly postsLastForever: boolean,
    public readonly postDurationNumber: number,
    public readonly postDurationUnit: DurationUnit,
    public readonly lineWidth: number,
    public readonly textBold: boolean,
    public readonly textItalic: boolean,
    public readonly textCaps: boolean,
    public readonly textSize: number,
    public readonly pattern: string,
    public readonly emailNotifications: boolean,
    public readonly giphyImages: GiphyImage[],
    public readonly realname: string) {}

  static defaultSticker: GiphyImage = {
    url: 'https://media1.giphy.com/media/3rgXBHrMCMTJsdc6C4/100w.gif',
    width: 100,
    height: 100
  };
  
  static defaultUserPrefs(): UserPrefs {
    return new UserPrefs(
      new Color(0, 100, 0),
      undefined,
      true,
      1,
      DurationUnit.day,
      10,
      false,
      false,
      false,
      16,
      'fill',
      false,
      [UserPrefs.defaultSticker],
      '');
  }
      
}
