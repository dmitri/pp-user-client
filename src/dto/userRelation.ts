import {UserRelationType, PublicUserInfo, DatabaseIdentity} from './postplaneDto';

export interface UserRelation extends DatabaseIdentity {
  readonly type: UserRelationType;
  readonly userinfo: PublicUserInfo;
  readonly toUserinfo: PublicUserInfo;
  readonly atTime: Date|Object;
}