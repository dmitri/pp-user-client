import {TypeSerializer, UserRelation, UserRelationTypeSerializer, PublicUserInfoSerializer} from './postplaneDto';

export class UserRelationSerializer implements TypeSerializer<UserRelation> {

  private readonly publicUserInfoSerializer = new PublicUserInfoSerializer();

  public serialize(n: UserRelation): {[s: string]: any} {
    return undefined;
  }
  

  public deserialize(json: {[s:string]: any}, databaseId?: string): UserRelation|undefined {
    const type = UserRelationTypeSerializer[json.type];
    if (! type) {
      console.error('Unknown UserRelationType: ', json.type);
      return undefined;
    }
    return {
      databaseId: databaseId,
      type: type,
      userinfo: this.publicUserInfoSerializer.deserialize(json.userinfo),
      toUserinfo: this.publicUserInfoSerializer.deserialize(json.toUserinfo),
      atTime: json.atTime
    }
  }
}
 