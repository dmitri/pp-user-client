const enum UserRelationType {
  FriendRequestSent = 'FriendRequestSent',
  FriendRequestReceived = 'FriendRequestReceived',
  Friends = 'Friends'
}

const UserRelationTypeSerializer: {[s: string]: UserRelationType} = {
  FriendRequestSent: UserRelationType.FriendRequestSent,
  FriendRequestReceived: UserRelationType.FriendRequestReceived, 
  Friends: UserRelationType.Friends
}

export {UserRelationType, UserRelationTypeSerializer}
