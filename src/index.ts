const firebase = require('firebase');
const request = require('request');
const parseString = require('xml2js').parseString;
const parser = require('xml2json');
const uuidv1 = require('uuid/v1');
const url = require('url');
const http = require('http');
const sizeOf = require('image-size');

import {Post, Position, PostSerializer} from './dto/postplaneDto';

const firebaseConfig = {
  apiKey: "AIzaSyB2BCwIEkxyzA9FPPmngWS2oy99P6TeKmM",
  authDomain: "postplane-v4.firebaseapp.com",
  databaseURL: "https://postplane-v4.firebaseio.com",
  storageBucket: "postplane-v4.appspot.com",
  projectId: "postplane-v4"
};

const liveJournalUrl = 'https://www.livejournal.com/stats/latest-img.bml';
const offset = new Position(0, 30);
const firebaseApp = firebase.initializeApp(firebaseConfig);
const planeName = 'found-lj-images';
const postSerializer = new PostSerializer();

function thenRequest(url): Promise<string> {
  return new Promise((resolve, reject) => {
    request(liveJournalUrl, { json: false }, (err, res, body) => {
      if (err) { 
        reject(err);
      } else {
        resolve(body);
      }
    });
  })
}

function thenRequestBuffer(url): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const requestSettings = { url: url, method: 'GET', encoding: null };
    request(requestSettings, (err, res, body) => {
      if (err) { 
        reject(err);
      } else {
        resolve(body);
      }
    });
  })
}

function thenXml(xml: string): Promise<any> {
  return new Promise((resolve, reject) => {
    // parseString(xml, function (err, result) {
    //   if (err) {
    //     reject(err);
    //   } else {
    //     resolve(result);
    //   }
    // });
    resolve(parser.toJson(xml, {
        object: true,
    }));
  })
}


function sizeFromUrl(imgUrl: string): Promise<any> {

  return new Promise((resolve, reject) => {
    return thenRequestBuffer(imgUrl)
      .then(buffer => {
        try {
          const sz = sizeOf(buffer);
          console.log(sz);
          resolve(sz);
        } catch (e) {
          reject(e);
        }
      });
    });
}

function recentImageToPost(
  userId: string, 
  planeName: string, 
  position: Position,
  size: Position,
  recentImageUrl: string,
  ): Post 
{

  const p = new Post();
  p.userId = userId;
  p.renderId = uuidv1();
  p.databaseId = uuidv1();
  p.planeName = planeName;
  p.text = recentImageUrl;
  p.position = position;
  p.size = size;

  return p;
}

function insertPost(post: Post): Promise<any> {
  return firebase.firestore()
    .collection('publicPlanes').doc(planeName)
    .collection('posts').doc(post.databaseId)
    .set(postSerializer.serialize(post));
}


firebase.auth().signInWithEmailAndPassword('foundimages@foundimages.com', 'foundimages!')
  .then(auth => {

    const userId = firebase.auth().currentUser.uid;

    return thenRequest(liveJournalUrl)
      .then(xml => thenXml(xml))
      .then(json => {

        return json['livejournal']['recent-images']['recent-image'].reduce((priorPost, currImg) => {

          return priorPost.then((post: Post) => {

            return sizeFromUrl(currImg.img)
              .then(sz => {

                const nextPosition = post 
                  ? (new Position(0, post.position.y + post.size.y + offset.y))
                  : new Position(0, 0);

                const nextPost = recentImageToPost(
                  userId, planeName, nextPosition, new Position(sz.width, sz.height),
                    currImg.img + ' ' + currImg.url);
                console.log('nextPost is ', nextPost);
                return insertPost(nextPost)
                  .then(() => nextPost);
              })
              .catch(err => {
                console.error('got err', currImg, err);
                return post;
              });
          })

      }, Promise.resolve(undefined));
    
    })
  }).then(() => {
    console.log('and were done');
    firebaseApp.delete();
  })


